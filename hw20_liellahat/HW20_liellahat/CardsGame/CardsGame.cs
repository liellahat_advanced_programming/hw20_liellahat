﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics;
using System.Drawing;

namespace CardsGame
{
    public partial class CardsGame : Form
    {
        private const int width = 100, height = 150, x = 50, y = 300, enemyX = 530, enemyY = 100;
        private const string path = @"C:\Users\User\Desktop\HW20_liellahat\CardsGame\PNG-cards\";
        private int points, enemyPoints;
        private bool exitFlag;
        private List<PictureBox> cardPics;
        private PictureBox enemyCardPic;
        private PictureBox lastCardPic;
        private Card card;
        private Card enemyCard;
        private NetworkStream clientStream;
        private Thread communication, exit;
       
        public CardsGame()
        {
            InitializeComponent();

            this.exitFlag = false;
            this.card = new Card();
            this.card.used = true;
            this.points = 0;
            this.enemyPoints = 0;
            this.label2.Text = "your score: " + this.points;
            this.label1.Text = "enemy score: " + this.enemyPoints;
            this.Enabled = false;
            
            this.communication = new Thread(Communication);
            this.communication.IsBackground = true;
            this.communication.Start();
            this.exit = new Thread(Exit);
            this.exit.IsBackground = true;
            this.exit.Start();
        }

        private void CardsGame_Load(object sender, EventArgs e)
        {

        }

        private void Communication()
        {
            try
            {
                var bufferIn = new byte[4];
                var bufferOut = new byte[4];
                string input;
                var encode = new ASCIIEncoding();
                var client = new TcpClient();
                var serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);  // the thread is killed if the connection failed.
                client.Connect(serverEndPoint);
                this.clientStream = client.GetStream();

                while (true)  // waiting for the game to start or end.
                {
                    this.clientStream.Read(bufferIn, 0, 4);
                    input = encode.GetString(bufferIn);

                    if (input == "0000")
                    {
                        Invoke((MethodInvoker)(() => { this.Enabled = true; }));
                        Invoke((MethodInvoker)(() => { GenerateCards(); }));
                        break;
                    }
                    if (input == "2000")
                    {
                        this.exitFlag = true;
                        communication.Abort();
                    }
                }

                while (true)
                {
                    input = "0000";

                    while (this.card.used) ;  // waits for the user to pick his card. this.card is initialized in c'tor to be used.
                    
                    card.used = true;
                    // the program crashes at the next line if the other client quits, because the there is no one in the other side. i found no way to make sure that the other client didn't quit, because
                    // after reading from the stream (to make sure the server didn't send a 2000) the data is lost. i cannot copy the stream by value.
                    this.clientStream.Write(encode.GetBytes("1" + this.card.Code), 0, 4); 
                    this.clientStream.Flush();

                    while (input == "0000")  // waits for the opponent to pick his card.
                    {
                        this.clientStream.Read(bufferIn, 0, 4);
                        input = encode.GetString(bufferIn);
                        if (input == "2000")  
                        {
                            this.exitFlag = true;
                            communication.Abort();
                        }
                    }
                    this.enemyCard = new Card(input);

                    Invoke((MethodInvoker)(() => { this.enemyCardPic.ImageLocation = CardsGame.path + this.enemyCard.FileName; }));

                    if (card > enemyCard)
                    {
                        this.points++;
                        Invoke((MethodInvoker)(() => { this.label2.Text = "your score: " + this.points; })); 
                    }
                    else if (card < enemyCard)
                    {
                        this.enemyPoints++;
                        Invoke((MethodInvoker)(() => { this.label1.Text = "enemy score: " + this.enemyPoints; }));
                    }
                }
            }
            catch (Exception e)
            {
                Invoke((MethodInvoker)(() => { MessageBox.Show(e.ToString()); }));  // doesn't send a 2000 message because the connection is not necessarily ready.
                Application.Exit();
            }
        }

        private void GenerateCards()
        {
            // the cards numbers are from left to right.

            this.cardPics = new List<PictureBox>();

            for (var i = 0; i < 10; i++)
            {
                this.cardPics.Add(InitPicture(CardsGame.path + "card_back_red.png", CardsGame.x + 120 * i, CardsGame.y));
                this.cardPics[i].Click += new EventHandler(PickCard);
            }

            this.enemyCardPic = InitPicture(CardsGame.path + "card_back_blue.png", CardsGame.enemyX, CardsGame.enemyY);

        }    
        
        private void PickCard(object sender, EventArgs e)
        {
            this.card = new Card();
            ((PictureBox)sender).ImageLocation = CardsGame.path + this.card.FileName;
            this.enemyCardPic.ImageLocation = CardsGame.path + "card_back_blue.png";

            if (this.lastCardPic != null)  // if it's not the first round.
            {
                this.lastCardPic.ImageLocation = CardsGame.path + "card_back_red.png";  // change the last card back to upside down.
            }
            this.lastCardPic = ((PictureBox)sender);
        }

        private PictureBox InitPicture(string picPath, int x, int y)
        {
            var pictureBox = new PictureBox
            {
                Name = "enemyCard",
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(CardsGame.width, CardsGame.height),
                Location = new Point(x, y)
            };
            this.Controls.Add(pictureBox);
            pictureBox.ImageLocation = picPath;

            return pictureBox;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.exitFlag = true;
        }

        private void Exit()  // runs in the background until the client wants to exit (or got a 2000 message), and then quits.
        {
            while (!this.exitFlag) ;
            if (communication.IsAlive)  // if 2000 msg has not been sent yet.
            {
                this.clientStream.Write(new ASCIIEncoding().GetBytes("2000"), 0, 4);
                this.clientStream.Flush();
            }
            Invoke((MethodInvoker)(() => { MessageBox.Show(this.points + " : " + this.enemyPoints); }));
            Application.Exit();
        }
    }
}
