﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsGame
{
    public class Card
    {
        public bool used { get; set; }
        public string FileName { get; set; }
        public string Code { get; set; }


        private string[] fileNums = { "ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king" };
        private string[] fileShapes = { "diamonds", "clubs", "spades", "hearts" };

        private string[] nums = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13" };
        private char[] shapes = { 'D', 'C', 'S', 'H' };

        public Card()
        {
            var rnd = new Random();

            var random1 = rnd.Next(0, 13);
            var random2 = rnd.Next(0, 4);

            this.used = false;
            this.Code = nums[random1] + shapes[random2];
            this.FileName = fileNums[random1] + "_of_" + fileShapes[random2] + ".png";
        }

        public Card(string message)
        {
            this.used = false;
            this.Code = message.Remove(0, 1);
            this.FileName = fileNums[Array.IndexOf(nums, Code.Substring(0, 2))] + "_of_" + fileShapes[Array.IndexOf(shapes, Code[2])] + ".png";  // creating FileName by finding the indexes of the Code's num and shape in the arrays.
        }

        public static bool operator >(Card left, Card right)
        {
            return int.Parse(left.Code.Substring(0, 2)) > int.Parse(right.Code.Substring(0, 2));
        }

        public static bool operator <(Card left, Card right)
        {
            return int.Parse(left.Code.Substring(0, 2)) < int.Parse(right.Code.Substring(0, 2));
        }

        public static bool operator ==(Card left, Card right)
        {
            return (left == null && right == null) || (int.Parse(left.Code.Substring(0, 2)) == int.Parse(right.Code.Substring(0, 2)));
        }

        public static bool operator !=(Card left, Card right)
        {
            return int.Parse(left.Code.Substring(0, 2)) != int.Parse(right.Code.Substring(0, 2));
        }
    }
}
